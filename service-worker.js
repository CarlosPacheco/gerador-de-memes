const CACHE_NAME = 'meme-generator-v1';
const INDEXED_DB_NAME = 'meme-generator-db-v1';
const INDEXED_DB_VERSION = 1;
const OBJECT_STORE_TEMPLATES = 'templates';

const urlsToCache = [
    '/index.html',
    '/assets/js/index.js',
    '/assets/js/jquery-3.4.1.min.js',
    '/assets/js/popper.min.js',
    '/assets/js/bootstrap.min.js',
    '/assets/css/bootstrap.min.css',
    '/assets/css/custom.css',
    '/assets/img/001.png',
    '/assets/img/002.png',
    '/assets/img/003.png',
    '/assets/img/004.png',
    '/assets/img/005.png'
];

const dbPromise = new Promise((resolve, reject) => {
    const idbRequest = indexedDB.open(INDEXED_DB_NAME, INDEXED_DB_VERSION);

    idbRequest.onerror = error => {
        console.log('Erro ao abrir o IndexedDB');
        console.log(error);
        reject(error)
    }

    idbRequest.onsuccess = evt => {
        const transaction = evt.target.result.transaction([OBJECT_STORE_TEMPLATES], 'readwrite');
        const objectStore = transaction.objectStore(OBJECT_STORE_TEMPLATES);
        transaction.onerror = error => {
            // console.log('Não foi possível salvar os templates no IndexedDB!');
            // console.log(error);
        }

        objectStore.add({codigo: 1, nome: '001.png'});
        objectStore.add({codigo: 2, nome: '002.png'});
        objectStore.add({codigo: 3, nome: '003.png'});
        objectStore.add({codigo: 4, nome: '004.png'});
        objectStore.add({codigo: 5, nome: '005.png'});

        resolve(evt.target.result)
    };

    idbRequest.onupgradeneeded = evt => {
        if(!evt.target.result.objectStoreNames.contains(OBJECT_STORE_TEMPLATES)) {
            evt.target.result.createObjectStore(OBJECT_STORE_TEMPLATES, {keyPath: 'codigo'});
        }
    };
});

self.addEventListener('install', (event) => {
    event.waitUntil(
        caches.open(CACHE_NAME).then(cache => cache.addAll(urlsToCache))
    );
    self.skipWaiting();
});

self.addEventListener('activate', event => {
    event.waitUntil(
        caches.keys().then(async cacheNames => {
            await dbPromise;

            if(Notification.permission == 'default') Notification.requestPermission().then(() => {});
            if(Notification.permission == 'granted') {
                const title = 'Uso Offline';
                const options = {
                    body: 'O site já pode ser usado em modo offline!',
                    lang: 'pt-BR'
                };
                // self.registration.showNotification(title, options);
            }

            return Promise.all(
                cacheNames.map(cacheName => {
                    if(cacheName !== CACHE_NAME) return caches.delete(cacheName);
                })
            );
        })
    );
});

self.addEventListener('fetch', event => {
    event.respondWith(
        caches.match(event.request).then(function(response) {
          return response || fetch(event.request);
        })
    );
});
