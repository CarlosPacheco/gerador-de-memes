const INDEXED_DB_NAME = 'meme-generator-db-v1';
const INDEXED_DB_VERSION = 1;
const OBJECT_STORE_TEMPLATES = 'templates';

if(!('serviceWorker' in navigator)) {
    alert('ServiceWorker não está disponível em seu navegador');
} else {
    window.addEventListener('load', () => {
        navigator.serviceWorker.register('service-worker.js')
            .then(registration => {
                let serviceWorker;
                if (registration.installing) {
                    serviceWorker = registration.installing;
                } else if (registration.waiting) {
                    serviceWorker = registration.waiting;
                } else if (registration.active) {
                    serviceWorker = registration.active;
                }
    
                if (serviceWorker) {
                    if (serviceWorker.state == 'activated') {
                        updateImages();
                    } else {
                        // Se o ServiceWorker não está ativado esperamos o evento de ativação ser lançado
                        serviceWorker.addEventListener('statechange', evt => {
                            if (evt.target.state == 'activated') updateImages();
                        });
                    }
                }
                console.log(`ServiceWorker registrado com o escopo: ${registration.scope}`);

            }, error => {
                alert('ServiceWorker não registrado');
                console.log(error);
            });
    });
}

function updateImages() {
    const idbRequest = indexedDB.open(INDEXED_DB_NAME, INDEXED_DB_VERSION);

    idbRequest.onsuccess = evt => {
        const db = evt.target.result;
        const transaction = db.transaction([OBJECT_STORE_TEMPLATES], 'readonly');
        const request = transaction.objectStore(OBJECT_STORE_TEMPLATES).getAll();

        transaction.onerror = error => {
            console.log('Não foi possível buscar os templates no IndexedDB!');
            console.log(error);
        }

        request.onsuccess = async result => {
            const templates = result.target.result;

            for(const template of templates) {
                $('#templates').append(`<div>
                    <img id="img-${template.codigo}" src="assets/img/${template.nome}" alt="Imagem de meme ${template.codigo}">
                </div>`);

                $(`#img-${template.codigo}`).click(() => {
                    const text = $('#input-meme-text').val();
                    if(typeof text == 'undefined' || text.length == 0) return alert('Digitar um texto para o meme!');

                    const img = $(`#img-${template.codigo}`);

                    $('main').attr('hidden', true);
                    $('#complete-meme').attr('hidden', false);
                    $('#btn-return').attr('hidden', false);

                    const canvas = $('#img-canvas')[0];
                    const context = canvas.getContext('2d');

                    canvas.width = img.width();
                    canvas.height = img.height();

                    context.drawImage(img.get(0), 0, 0, 600, 650, 0, 0, canvas.width, canvas.height);
                    context.font = "12px Arial";
                    context.fillStyle = "black";

                    context.fillText(text, 0, 17, canvas.width);

                    $('#download-meme').attr('href', canvas.toDataURL('image/png'));
                });
            }
        }; 
    };
}

$('#btn-return').click(() => {
    $('main').attr('hidden', false);
    $('#complete-meme').attr('hidden', true);
    $('#btn-return').attr('hidden', true);
});
